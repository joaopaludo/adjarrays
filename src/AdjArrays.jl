module AdjArrays

include("../operadores/Utils.jl")
include("../operadores/Conversor.jl")
include("../operadores/Medidas.jl")
#include("../operadores/Plot.jl")

include("../arquiteturas/Global.jl")
include("../arquiteturas/Regular.jl")
include("../arquiteturas/Random.jl")
include("../arquiteturas/SmallWorld.jl")
include("../arquiteturas/Plasticidade.jl")

include("../operadores/SmallWorldNess.jl")


export Utils
export Conversor
export Medidas
export Plot
export SmallWorldNess

export Global
export Regular
export Random
export SmallWorld


end
