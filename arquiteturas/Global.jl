#Gera uma rede global com N elementos

module Global


function adjMat(N::Int)
	return [ Int(i != j) for i in 1:N, j in 1:N ]
end


function adjVet(N::Int)
	adjVet = Int[]

	for i in 0:N-1, j in 0:N-1
		(i == j) && continue

		push!(adjVet, i*N + j)
	end

	return adjVet
end


function list()
	println("Funções de Global:\n")

	println("adjMat(N::Int)")
	println("adjVet(N::Int)")
end

end