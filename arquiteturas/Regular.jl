#Gera uma rede regular com os seguintes atributos
#N: Número de elementos na rede
#k: Número de vizinhos de cada neurônio (grau de conectivadade)

module Regular

using ..Utils
 

function adjMat(N::Int, k::Int)
	adjMat = zeros(Int, N, N);

	#Checagem de Erro
	(k > N-1) && (k = N-1)

	#Preenche a matriz
	for i in 1:N
		c = 1
		
		#Gera ligações simétricas para esquerda e direita
		while 2c <= k
			adjMat[i, i_plus_x(i, c, N)] = 1
			adjMat[i, i_plus_x(i,-c, N)] = 1
			
			c += 1
		end
		
		#Põe a ligação restante (para quando k é ímpar)
		if k == 2c - 1
			adjMat[i, i_plus_x(i, c, N)] = 1
		end		
	end

	return adjMat
end

function adjVet(N::Int, k::Int)
	adjVet   = zeros(Int, N*k)
	vizinhos = zeros(Int, k)

	halfk = Int(floor(k/2))
	idxAdjVet = 1

	#Checagem de Erro
	(k > N-1) && (k = N-1)
	
	#Para cada neurônio
	for i in 1:N				
		#Encontra e guarda os vizinhos simétricos de i
		c = 1
		while 2c <= k
			vizinhos[c]         = i_plus_x(i, c, N) - 1  #Tira 1 para por em notação de vetor começando em 0
			vizinhos[c + halfk] = i_plus_x(i,-c, N) - 1
			
			c += 1
		end

		#Põe a ligação restante (para quando k é ímpar)
		if k == 2c - 1
			vizinhos[end] = i_plus_x(i, c, N) - 1
		end
		
		#Ordena os vizinhos
		sort!(vizinhos)

		#Passa os vizinhos de i para o adjVet
		for j in vizinhos
			adjVet[idxAdjVet] = (i-1)*N + j
			idxAdjVet += 1
		end
	end

	return adjVet
end


function list()
	println("Funções de Regular:\n")

	println("adjMat(k::Int)")
	println("adjVet(k::Int)")
end

end
