module Plasticidade


function randomWeightVet(N::Int; minWeight=0.0, maxWeight=1.0)
	dif = maxWeight - minWeight;
	
	#Checagem de Erros
	if N < 1
		println("Erro! O vetor de pesos deve ter pelo menos um elemento")
		return
	end
	
	vet = [minWeight + rand() * dif  for i in 1:N];
end


function incompleteWeightVet(N::Int, numSynapses::Int; minWeight=0.0, maxWeight=1.0)
	dif = maxWeight - minWeight;
	
	#----- Checagem de Erros -----
	if N < 1
		println("Erro! O vetor de pesos deve ter pelo menos um elemento")
		return
	end
	
	if numSynapses > N  ||  numSynapses < 0
		println("Erro! Número de sinápses inválido!")
		return
	end
	#-----------------------------
	
	synIndices = zeros(Int, numSynapses);
	vet = zeros(N);
	idx = 0
	
	for i in 1:numSynapses
		invalido = true
		
		#Encontra índices das sinapses que será mudada
		while invalido
			invalido = false
			idx = Int(rand(UInt) % N) + 1
			
			for j in 1:i
				if synIndices[j] == idx
					invalido = true
					break
				end
			end
		end
		
		#Anota que essa sinapse já foi mudada
		synIndices[i] = idx
		
		#Acha valor da sinapse
		vet[idx] = minWeight + rand() * dif
	end
	
	return vet
end



weightVet(N::Int; minWeight=0.0, maxWeight=1.0) = randomWeightVet(N, minWeight=minWeight, maxWeight=maxWeight);



export randomWeightVet
export incompleteWeightVet
export weightVet

end
