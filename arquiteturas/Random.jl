#Cria uma rede aleatória direcionada

#N: Número de elementos da rede
#M: Número de conexões  da rede

module Random

using ..Utils
using ..Conversor
using LightGraphs

#=----- Feito por mim -----
#Gera uma rede aleatória direcionada com N elementos e M conexões pelo método de Erdös-Renyi
function adjMat_ER(N::Int, M::Int)
	#O número máximo de conexões possível é o de uma rede global
	if M > N*(N-1)
		println("Warning: Número de conexões maior que N(N-1) foi redefinido para N(N-1)!")
		M = N*(N-1)
	end

	(M < 0) && println("Warning: Número de conexões menor que 0 foi redefinido para 0!")


	mat = zeros(Int, N, N)

	m = 0
	#Gera as conexões
	while m < M
		i = Int(floor( rand()*N )) + 1
		j = Int(floor( rand()*N )) + 1

		if (i != j) && (mat[i,j] == 0)
			mat[i,j] = 1
			m += 1
		end
	end

	return mat
end

function adjVet_ER(N::Int, M::Int)
	#O número máximo de conexões possível é o de uma rede global
	if M > N*(N-1)
		println("Warning: Número de conexões maior que N(N-1) foi redefinido para N(N-1)!")
		M = N*(N-1)
	end

	(M < 0) && println("Warning: Número de conexões menor que 0 foi redefinido para 0!")

	vet = []

	#Gera as conexões restantes aleatoriament
	for m in 1:M
		while true
			i = Int(floor( rand()*N ))
			j = Int(floor( rand()*N ))

			if (i != j) && ( !isInArray(vet, i*N + j) )
				push!(vet, i*N + j)
				break
			end
		end
	end

	sort!(vet)

	return vet
end

#Gera uma rede aleatória direcionada com N elementos e M conexões pelo método de Erdös-Renyi
function adjMat_ER(N::Int, p::Float64)
	#O p deve estar entre 0 e 1
	if p > 1.0
		println("Warning: p maior que 1.0 foi redefinido para 1.0!")
		p = 1.0
	end

	(p < 0.0) && println("Warning: p menor que 0.0 foi redefinido para 0.0!")


	mat = zeros(Int, N, N)
		
	#Gera as conexões
	for i in 1:N, j in 1:N
		(i == j) && continue

		(rand() < p) && (mat[i][j] = 1)
	end

	return mat
end

function adjVet_ER(N::Int, p::Float64)
	#O p deve estar entre 0 e 1
	if p > 1.0
		println("Warning: p maior que 1.0 foi redefinido para 1.0!")
		p = 1.0
	end

	(p < 0.0) && println("Warning: p menor que 0.0 foi redefinido para 0.0!")

	vet = []

		#Gera as conexões
	for i in 1:N, j in 1:N
		(i == j) && continue
		
		(rand() < p) && push!(vet, i*N + j)
	end

	sort!(vet)

	return vet
end


#Define método padrão
adjMat = adjMat_ER
adjVet = adjVet_ER
-------------------------=#

function adjMat(N::Integer, p::Float64; dense=false, seed=-1) 
	adj = adjacency_matrix(erdos_renyi(N, p, is_directed=true, seed=seed));
	
	(dense)  ||  (return adj)
	(dense)  &&  (return Matrix(adj))
end

adjVet(N::Integer, p::Float64; seed=-1) = adjMatToVet( Matrix(adjacency_matrix( erdos_renyi(N, p, is_directed=true, seed=seed) )) );

function adjMat(N::Integer, M::Integer; dense=false, seed=-1)
	adj = adjacency_matrix(erdos_renyi(N, M, is_directed=true, seed=seed));
	
	(dense)  ||  (return adj)
	(dense)  &&  (return Matrix(adj))
end

adjVet(N::Integer, M::Integer; seed=-1) = adjMatToVet( Matrix(adjacency_matrix( erdos_renyi(N, M, is_directed=true, seed=seed) )) );
	


function list()
	println("Funções de Random:\n")
	
	#=
	println("adjMat_ER(N::Int, M::Int)")
	println("adjVet_ER(N::Int, M::Int)")
	println("adjMat_ER(N::Int, p::Float64)")
	println("adjVet_ER(N::Int, p::Float64)")
	=#
	
	println("adjMat(N::Int, M::Int; dense=false, seed=-1)")
	println("adjVet(N::Int, M::Int; seed=-1)")
	println("adjMat(N::Int, p::Float; dense=false, seed=-1")
	println("adjVet(N::Int, p::Float; seed=-1)")
end

end
