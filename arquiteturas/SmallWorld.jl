#Cria uma rede do tipo small world direcionada

#N: Número de elementos da rede
#p: Proporção/probabilidade de atalhos na rede
#k: Números de vizinhos de cada neurônio da rede regular inicial

module SmallWorld


using DelimitedFiles
using ..Utils
using ..Regular
using LightGraphs
import Random.seed!

function adjMat_NewmanWatts(N::Int, k::Int, p::Real; seed=-1)
	#Checagem de Erro
	if p > 1.0 || p < 0.0
		println("Proporção de Atalhos invalida!")
		return
	end
	
	(seed == -1)  ||  seed!(seed);
	
	#Partindo de uma rede regular
	mat = Regular.adjMat(N, k);

	#=	numMaxAtalhos = Número de atalhos a serem adicionados quando p=1
	    Lembrando: N(N-1) = Conexões de uma rede global  direcionada
	                   Nk = Conexões de uma rede regular direcionada
	=#
	#numMaxAtalhos = N*(N-1) - N*k;	#Versão do método em que p=1 => global
	numMaxAtalhos = N*k;	#Versão original do método
	
	numAtalhos = Int( round(p * numMaxAtalhos) )

	#Põe os atalhos
	for c in 1:numAtalhos
		i=0; j=0;

		#Acha um par i-j que não esteja já conectado
		while true
			i = Int(floor( rand()*N )) + 1
			j = Int(floor( rand()*N )) + 1

			(i != j) && (mat[i,j] == 0) && break
		end

		#Conecta i e j
		mat[i,j] = 1
	end

	return mat
end

function adjMat_WattsStrogatz(N::Int, k::Int, p::Real; dense=false, seed=-1)
	#Checagem de Erro
	if p > 1.0 || p < 0.0
		println("Porcentagem de Atalhos invalida!")
		return
	end

	adjMat = adjacency_matrix(watts_strogatz(N, k, p, seed=seed, is_directed=true))

	#=--- Feito por mim ---
		#Partindo de uma rede regular
	mat = Regular.adjMat(N, k);


		#Para cada elemento
	for i in 1:N
			#Para cada um de seus vizinhos imediatos
		for vizinho in [i_plus_x(i,c,N) for c in -k:k]

				#Ignora o loop no qual o vizinho de i é o próprio i
			(vizinho == i) && continue

				#Com uma probabilidade p, tira a conexão de i com esse vizinho e liga i a de um outro elemento da rede
			if rand() < p
				j=0;

					#Acha um j que não esteja já conectado com i
				while true
					j = Int(floor( rand()*N )) + 1
					(i != j) && (mat[i,j] == 0) && break
				end

					#Troca as conexões
				mat[i, vizinho] = 0
				mat[i, j] = 1
			end
		end
	end
	---------------------=#

	dense  ||  (return adjMat)
	dense  &&  (return Matrix(adjMat))
end


function adjVet_NewmanWatts(N::Int, k::Int, p::Real; seed=-1)
	#Checagem de Erro
	if p > 1.0 || p < 0.0
		println("Proporção de Atalhos invalida!")
		return
	end
	
	(seed == -1) || seed!(seed);

	#=	numMaxAtalhos = Número de atalhos a serem adicionados quando p=1
	    Lembrando: N(N-1) = Conexões de uma rede global  direcionada
	                   Nk = Conexões de uma rede regular direcionada
	=#
	#numMaxAtalhos = N*(N-1) - N*k;	#Versão do método em que p=1 => global
	numMaxAtalhos = N*k;	#Versão original do método
	
	numAtalhos = Int( round(p * numMaxAtalhos) )

	#Partindo de uma rede regular
	vet = Regular.adjVet(N, k)

	for c in 1:numAtalhos
		#Acha uma conexão válida
		conexao = 0
		while true
			i = Int(floor( rand()*N ))
			j = Int(floor( rand()*N ))
			conexao = i*N + j

			(i != j) && !isInArray(vet, conexao) && break
		end

		#Guarda a conexão num array
		push!(vet, conexao)
	end

	#Ordena o vetor de adjacencia
	sort!(vet)

	return vet
end


function adjVet_WattsStrogatz(N::Int, k::Int, p::Real; seed=-1)
	#Checagem de Erro
	if p > 1.0 || p < 0.0
		println("Porcentagem de Atalhos invalida!")
		return
	end
	
	(seed == -1) || seed!(seed);

	vet = Int[]
	halfk = Int(floor(k/2))
	
	#Range com as posições dos vizinhos imediatos de i em relação a i 
	vizinhoDistRange = (k%2 == 0) ? (-halfk:halfk) : (-halfk:halfk+1)

	#Para cada elemento
	for i in 0:N-1
		#Para cada um de seus vizinhos imediatos
		for vizinho in [i_plus_x(i,c,N)%N for c in vizinhoDistRange]

			#Ignora o loop no qual o vizinho de i é o próprio i
			(vizinho == i) && continue

			#Com uma probabilidade p, tira a conexão de i com esse vizinho e liga i a de um outro elemento da rede
			if rand() < p
				j=0;

				isValid = false
				#Acha um j que não esteja já conectado com i
				while !isValid
					isValid = true
					j = Int(floor( rand()*N ))

					#Checa se j já é um dos vizinhos imediatos de i, ou o proprio i
					for m in [i_plus_x(i,q,N)%N for q in -k:k]
						(j == m) && (isValid = false)
					end

					#Checa se i já está conectado com j
					isInArray(vet, i*N+ j) && (isValid = false)
				end

				#Conecta i e j
				push!(vet, i*N + j)
			else

				push!(vet, i*N + vizinho)
			end
		end
	end

	sort!(vet)

	return vet
end



#Define Newman-Watts como a maneira padrão
adjMat(N::Int, k::Int, p::Real; seed=-1) = adjMat_WattsStrogatz(N, k, p, seed=seed)
adjVet(N::Int, k::Int, p::Real; seed=-1) = adjVet_WattsStrogatz(N, k, p, seed=seed)


#Funções para salvar adjArrays de maneira prática em arquivos
function salvaMatPadrao(N::Int, k::Int, p::Real; seed=-1)
	filePath = "adjMatSW_n$(N)_p$p.dat";

	mat = adjMat(N, k, p, seed=seed);

	writedlm(filePath, mat, ' ');
end

function salvaVetPadrao(N::Int, k::Int, p::Real; seed=-1)
	filePath = "adjVetSW_n$(N)_p$p.dat";

	vet = adjVet(N, k, p, seed=seed);

	writedlm(filePath, vet, '\n');
end


#Gera várias redes mundo pequeno no intervalo de p passado e as salva
function geraAdjMats(N::Int, k::Int, p::Array{Float64, 1})
	for x in p
		salvaMatPadrao(N, k, x)
	end
end


function geraAdjVets(N::Int, k::Int, p::Array{Float64, 1})
	for x in p
		salvaVetPadrao(N, k, x)
	end
end




function list()
	println("Funções de SmallWorld:\n")

	println("adjMat_NewWatts(N::Int, k::Int, p::Real, seed=-1)")
	println("adjVet_NewWatts(N::Int, k::Int, p::Real, seed=-1)")
	println("adjMat_WattsStr(N::Int, k::Int, p::Real, dense=false, seed=-1)")
	println("adjVet_WattsStr(N::Int, k::Int, p::Real, seed=-1)")
	println("adjMat(N::Int, k::Int, p::Real, seed=-1)")
	println("adjVet(N::Int, k::Int, p::Real, seed=-1)")
	println("")
	println("salvaMatPadrao(N::Int, k::Int, p::Real, seed=-1)")
	println("salvaVetPadrao(N::Int, k::Int, p::Real, seed=-1)")
	println("")
	println("geraAdjMats(N::Int, k::Int, p::Array{Float64, 1})")
	println("geraAdjVets(N::Int, k::Int, p::Array{Float64, 1})")
end

end
