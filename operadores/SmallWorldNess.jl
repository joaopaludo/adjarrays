module SmallWorldNess

using ..Random
using ..SmallWorld
using ..Medidas
using DelimitedFiles
using Plots


#=****** ESSES RESULTADOS NÃO SÃO MAIS VÁLIDOS ******
#Retorna intervalo dos p que geram redes small-world de fato
function p_validos()
	#É considerada uma rede small-world uma rede construída pelo método Newman-Watts e que:
	#       -Tem livre caminho médio no máximo 5% maior que a rede aleatória correspondente
	#       -Tem clustering coefficient no mínimo 2 vezes maior que a rede aleatória correspondente

	result = Array{Any,2}(["N" "p mínimo" "p máximo"])

	result = vcat(result, [50    0.06   0.07])
	result = vcat(result, [1000  0.0037 0.0135])
	result = vcat(result, [5000  0.001  0.005])
	result = vcat(result, [10000 0.0005 0.003])
	result = vcat(result, [15000 0.0003 0.0025])
	result = vcat(result, [20000 0.0003 0.002])

	return result
end
=#


#----- Calcula Medidas -----
function medidasRandom(N::Int, numConexoes::Array{Int, 1}, numAmostras::Int, roubado::Bool=false)
	result = zeros(length(numConexoes), 3)

	for i in eachindex(numConexoes)
		somaLCM = 0.0
		somaCC  = 0.0
			
		for j in 1:numAmostras
			adjMat = Random.adjMat(N, numConexoes[i], dense=true)

			lcMedio = livreCaminhoMedio(adjMat)
			#(roubado)  && (lcMedio = Medidas.livreCaminhoMedio_roubado(adjMat))

			somaLCM += lcMedio
			somaCC  += clusteringCoefficient(adjMat)
		end

		result[i,1] = numConexoes[i]
		result[i,2] = somaLCM / numAmostras
		result[i,3] = somaCC  / numAmostras
	end

	return result
end

function medidasSW(N::Int, p::Array{Float64, 1}, numAmostras::Int)
	result = zeros(length(p), 3)

	for i in eachindex(p)
		somaLCM = 0.0
		somaCC  = 0.0

		for j in 1:numAmostras
			adjMat = SmallWorld.adjMat(N, 4, p[i])

			somaLCM += livreCaminhoMedio(Matrix(adjMat))
			somaCC  += clusteringCoefficient(Matrix(adjMat))
		end

		result[i,1] = p[i]
		result[i,2] = somaLCM / numAmostras
		result[i,3] = somaCC  / numAmostras
	end

	return result
end


#----- Intervalo Válido ---------

#Retorna um Tuple com o intervalo de p no qual a rede é small-world
function intervaloValido(randMeds::Array{Float64, 2}, swMeds::Array{Float64, 2}, razaoMaximaLCM, razaoMinimaCC)
	minimo = swMeds[1,1]
	maximo = swMeds[end,1]
	minId::Int = 0

	razaoLCM = swMeds[:,2] ./ randMeds[:,2]
	razaoCC  = swMeds[:,3] ./ randMeds[:,3]

	#Para cada p
	for i in eachindex(swMeds[:,1])
		if razaoLCM[i] <= razaoMaximaLCM && razaoCC[i] >= razaoMinimaCC
			minimo = swMeds[i,1]
			minId = i
			break
		end
	end

	(minId == 0) && return (0,0)

	for i in minId+1:length(swMeds[:,1])
		if razaoLCM[i] > razaoMaximaLCM || razaoCC[i] < razaoMinimaCC
			maximo = swMeds[i-1,1]
			break
		end
	end

	return (minimo, maximo)
end

function plotIntervaloValido(randMeds::Array{Float64, 2}, swMeds::Array{Float64, 2}, razaoMaximaLCM, razaoMinimaCC, yPlotRange=(0.5, 4))
	iv = intervaloValido(randMeds, swMeds, razaoMaximaLCM, razaoMinimaCC)

	lc = swMeds[:,2] ./ randMeds[:,2]
	cc = swMeds[:,3] ./ randMeds[:,3]

	plot(swMeds[:,1], [lc,cc], label=["lcm / lcm_r", "cc / cc_r"], xlab="p", ylims=yPlotRange)

	plot!([iv[1], iv[1]], [yPlotRange[1], yPlotRange[2]], label="p mínimo")
	plot!([iv[2], iv[2]], [yPlotRange[1], yPlotRange[2]], label="p máximo")
end


#----- Plots de Comparação ------

function plotCompare(randMeds::Array{Float64, 2}, swMeds::Array{Float64, 2})
	plcm = plot(randMeds[:,1], [randMeds[:,2], swMeds[:,2]], label=["Rand", "SW"], ylab="lcm");
	pcc  = plot(randMeds[:,1], [randMeds[:,3], swMeds[:,3]], label=["Rand", "SW"], ylab="cc", xlab="Num Conexoes");

	plot(plcm, pcc, layout=(2,1))
end

function plotCompare2(randMeds::Array{Float64, 2}, swMeds::Array{Float64, 2}, yPlotRange=(0.5, 4))
	lc = swMeds[:,2] ./ randMeds[:,2]
	cc = swMeds[:,3] ./ randMeds[:,3]

	plot(swMeds[:,1], [lc,cc], label=["lcm / lcm_r", "cc / cc_r"], xlab="p", ylims=yPlotRange)
end



function list()
	println("Funções em AdjArrays.SmallWorldNess:\n")

	println("p_validos()\n")

	println("medidasRandom(N::Int, numConexoes::Arrays{Int, 1}, numAmostras::Int, roubado::Bool=false)")
	println("medidasSW(N::Int, p::Float64, numAmostras::Int)\n")

	println("intervaloValido(randMeds::Array{Float64, 2}, swMeds::Array{Float64, 2}, razaoMaximaLCM, razaoMinimaCC)")
	println("plotIntervaloValido(randMeds::Array{Float64, 2}, swMeds::Array{Float64, 2}, razaoMaximaLCM, razaoMinimaCC, yPlotRange=(0.5, 4))")
	println("plotCompare(randMeds::Array{Float64, 2}, swMeds::Array{Float64, 2})")
	println("plotCompare2(randMeds::Array{Float64, 2}, swMeds::Array{Float64, 2}, yPlotRange=(0.5, 4))")
end


end
