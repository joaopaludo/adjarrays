module Medidas

using ..Conversor
using LightGraphs, SimpleWeightedGraphs


function livreCaminhoMedio(adjMat::Array{Int,2})
	g = SimpleDiGraph(adjMat)

	return livreCaminhoMedio(g)
end

function livreCaminhoMedio(adjVet::Array{Int,1}, N::Int)
	g = SimpleDiGraph(adjVetToMat(adjVet, N))

	return livreCaminhoMedio(g)
end


function livreCaminhoMedio(adjMat::Array{Float64,2})
	g = SimpleWeightedDiGraph(adjMat)

	return livreCaminhoMedio(g)
end

function livreCaminhoMedio(g::SimpleDiGraph)
	lcMedio::Float64 = 0
	N = nv(g)

	for i in 1:N
			#Vetor com a distância de i até todos os outros elementos
		dist = dijkstra_shortest_paths(g, i).dists

			#Adiciona a distância entre i e j. a_star() retorna o mínimo caminho
		for x in dist
			lcMedio += x
		end
	end

	return lcMedio / ( N*(N-1) )
end

function livreCaminhoMedio(g::SimpleWeightedDiGraph)
	lcMedio::Float64 = 0
	N = nv(g)

	for i in 1:N
			#Vetor com a distância de i até todos os outros elementos
		dist = dijkstra_shortest_paths(g, i).dists

			#Adiciona a distância entre i e j. a_star() retorna o mínimo caminho
		for x in dist
			lcMedio += x
		end
	end

	return lcMedio / ( N*(N-1) )
end


#=
function livreCaminhoMedio_roubado(adjMat::Array{Int,2})
	g = SimpleDiGraph(adjMat)

	lcMedio::Float64 = 0
	numSomados::Int = 0
	N = nv(g)

	for i in 1:N
			#Vetor com a distância de i até todos os outros elementos
		dist = dijkstra_shortest_paths(g, i).dists

			#Adiciona a distância entre i e j. a_star() retorna o mínimo caminho
		for x in dist
			if x < 1e10
				lcMedio += x
				numSomados += 1
			end
		end
	end

	return lcMedio / numSomados
end
=#


function clusteringCoefficient(adjMat::Array{Int,2})
	g = SimpleDiGraph(adjMat)

	return clusteringCoefficient(g)
end

function clusteringCoefficient(adjVet::Array{Int,1}, N::Int)
	g = SimpleDiGraph(adjVetToMat(adjVet, N))

	return clusteringCoefficient(g)
end

function clusteringCoefficient(g::SimpleDiGraph)
	c = 0
    ntriangles = 0

    for v in vertices(g)
        neighs = neighbors(g, v)

        for i in neighs, j in neighs
            i == j && continue

            ntriangles += 1

            if has_edge(g, i, j)
                c += 1
            end
        end
    end

    ntriangles == 0 && return 1.

	return c / ntriangles
end

#Clustering Coefficient de uma rede com pesos (segundo Zhang and Horvath 2005 Stat. Appl. Genet. Mol. Biol. 4(1) 1128)
function clusteringCoefficient(adjMat::Array{Float64,2})
	soma = 0.0
	
	#Calcula o CC de cada neurônio
	for i in 1:size(adjMat)[1]
		#-- Calcula C_in do neuronio i ---
	    neighs = neighbors(g, i);
	    
	    numerador = 0.0
	    for j in neighs, k in neighs
	    	numerador += adjMat[i,j] * adjMat[i,k] * (adjMat[j,k] + adjMat[k,j]) / 2
	    end
		
		denominador = sum(adjMat[i,:])^2 - sum( adjMat[i,:].^2 )
		
		Cin = numerador / denominador
		
		#Corrige erro
		(denominador == 0.0) && (Cin = 0.0)
		
		
		#-- Calcula C_out do neuronio i ---
		numerador = 0.0
	    for j in neighs, k in neighs
	    	numerador += adjMat[j,i] * adjMat[k,i] * (adjMat[j,k] + adjMat[k,j]) / 2
	    end
		
		denominador = sum(adjMat[:,i])^2 - sum( adjMat[:,i].^2 )
		
		Cout = numerador / denominador
		
		#Corrige erro
		(denominador == 0.0) && (Cout = 0.0)
		
		#-- Calcula CC do neurônio i e adiciona a soma ---
		soma += (Cin + Cout) / 2
	end
	
	return soma / size(adjMat)[1]
end

function clusteringCoefficient(g::SimpleWeightedDiGraph)
	clusteringCoefficient( Matrix(g.weights) )
end



function numConexoes(adjMat::Array{Int, 2})
	N = size(adjMat)[1]
	soma = 0

	for x in adjMat
		(x == 1) && (soma += 1)
	end

	return soma
end

function numConexoes(adjVet::Array{Int, 1})
	return length(adjVet)
end



export livreCaminhoMedio
export clusteringCoefficient
export numConexoes



function list()
	println("Funções de Medidas:\n")

	println("livreCaminhoMedio(adjMat::Array{Int,2})")
	println("livreCaminhoMedio(adjVet::Array{Int,2}, N::Int)")
	println("livreCaminhoMedio(g::SimpleDiGraph)")
	println("livreCaminhoMedio_roubado(adjMat::Array{Int,2})")
	println("")
	println("clusteringCoefficient(adjMat::Array{Int,2})")
	println("clusteringCoefficient(adjVet::Array{Int,1}, N::Int)")
	println("clusteringCoefficient(g::SimpleDiGraph)")
	println("")
	println("numConexoes(adjMat::Array{Int, 2})")
	println("numConexoes(adjVet::Array{Int, 1})")
end


end
