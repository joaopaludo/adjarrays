module Conversor

using DelimitedFiles: readdlm, writedlm


#---------- Variáveis do REPL ----------
function adjVetToMat(adjVet, N::Int)
	adjMat = zeros(Int, N, N);

	let i = 0;

		for idx in adjVet
			j = idx - i*N;

			while j >= N
				i += 1;
				j -= N;
			end

			global adjMat[i+1, j+1] = 1
		end
	end

	return adjMat
end

function adjMatToVet(adjMat::Array{Int,2})
	N = size(adjMat)[1];
	adjVet = zeros(Int, 0);

	for i in 1:N, j in 1:N
		if adjMat[i,j] == 1
			push!(adjVet, (i-1)*N + (j-1))
		end
	end

	return adjVet
end
#---------------------------------------


#--------- Diretamente por Arquivos ----------
function adjVetToMat(inFile::String, outFile::String, N::Int)
	adjVet = readdlm(inFile, Int)

	adjMat = adjVetToMat(adjVet, N)

	writedlm(outFile, adjMat, ' ')
end

function adjMatToVet(inFile::String, outFile::String)
	adjMat = readdlm(inFile, Int);

	adjVet = adjMatToVet(adjMat)

	writedlm(outFile, adjVet, '\n')
end


export adjMatToVet, adjVetToMat



function list()
	println("Funções de Conversor:\n")
	
	println("adjVetToMat(adjVet, N::Int)")
	println("adjMatToVet(adjMat::Array{Int,2})")
	println("adjVetToMat(inFile::String, outFile::String, N::Int)")
	println("adjMatToVet(inFile::String, outFile::String)")
end

end