module Plot

using LightGraphs
using GraphPlot
using DelimitedFiles
using ..Conversor
using Compose
import Cairo


function plotAdjMat(adjMat::Array{Int, 2})
	g = SimpleDiGraph(adjMat)
	gplothtml(g, layout=circular_layout, arrowlengthfrac=0)
end

function plotAdjMat(filePath::String)
	adjMat = readdlm(filePath, Int)
	g = SimpleDiGraph(adjMat)
	
	adjMat = 0

	gplothtml(g, layout=circular_layout, arrowlengthfrac=0)
end


function plotAdjVet(adjVet::Array{Int, 1}, N::Int)
	adjMat = adjVetToMat(adjVet, N)
	g = SimpleDiGraph(adjMat)

	adjMat = 0

	gplothtml(g, layout=circular_layout, arrowlengthfrac=0)
end

function plotAdjVet(filePath::String, N::Int)
	adjVet = readdlm(filePath, Int)

	adjMat = adjVetToMat(adjVet, N)
	g = SimpleDiGraph(adjMat)

	adjMat = 0
	
	gplothtml(g, layout=circular_layout, arrowlengthfrac=0)
end


function plotGraph(g::AbstractGraph)
	gplothtml(g, layout=circular_layout, arrowlengthfrac=0)
end




function saveAdjMatPlot(adjMat::Array{Int, 2}, filePath::String, size=(16cm, 16cm))
	g = SimpleDiGraph(adjMat)
	p = gplot(g, layout=circular_layout, arrowlengthfrac=0)

	draw(PNG(filePath, size[1], size[2]), p)
end

function saveAdjMatPlot(inFilePath::String, outFilePath::String, size=(16cm, 16cm))
	adjMat = readdlm(inFilePath, Int)
	g = SimpleDiGraph(adjMat)
	
	adjMat = 0

	p = gplot(g, layout=circular_layout, arrowlengthfrac=0)

	draw(PNG(outFilePath, size[1], size[2]), p)
end


function saveAdjVetPlot(adjVet::Array{Int, 1}, N::Int, filePath::String, size=(16cm, 16cm))
	adjMat = adjVetToMat(adjVet, N)
	g = SimpleDiGraph(adjMat)

	adjMat = 0

	p = gplot(g, layout=circular_layout, arrowlengthfrac=0)

	draw(PNG(filePath, size[1], size[2]), p)
end

function saveAdjVetPlot(inFilePath::String, N::Int, outFilePath::String, size=(16cm, 16cm))
	adjVet = readdlm(inFilePath, Int)

	adjMat = adjVetToMat(adjVet, N)
	g = SimpleDiGraph(adjMat)

	adjMat = 0
	
	p = gplot(g, layout=circular_layout, arrowlengthfrac=0)

	draw(PNG(outFilePath, size[1], size[2]), p)
end


function saveGraphPlot(g::AbstractGraph, filePath::String, size=(16cm, 16cm))
	p = gplot(g, layout=circular_layout, arrowlengthfrac=0)

	draw(PNG(filePath, size[1], size[2]), p)
end



export plotAdjVet
export plotAdjMat
export plotGraph

export saveAdjVetPlot
export saveAdjMatPlot
export saveGraphPlot



function list()
	println("Funções de Plot:\n")

	println("plotAdjMat(adjMat::Array{Int, 2})")
	println("plotAdjMat(filePath::String)")
	println("plotAdjVet(adjVet::Array{Int, 1}, N::Int)")
	println("plotAdjVet(filePath::String, N::Int)")
	println("plotGraph(g::AbstractGraph)")
	println("")
	println("saveAdjMatPlot(adjMat::Array{Int, 2}, filePath::String, size=(16cm, 16cm))")
	println("saveAdjMatPlot(inFilePath::String, outFilePath::String, size=(16cm, 16cm))")
	println("saveAdjVetPlot(adjVet::Array{Int, 1}, N::Int, filePath::String, size=(16cm, 16cm))")
	println("saveAdjVetPlot(inFilePath::String, N::Int, outFilePath::String, size=(16cm, 16cm))")
	println("saveGraphPlot(g::AbstractGraph, filePath::String, size=(16cm, 16cm))")
end


end
