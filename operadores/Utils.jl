module Utils


#Avança (ou retrocede) i em x unidades, fazendo N+1 = 1  e  1-1 = N
function i_plus_x(i, x, N)
	i = (i+N+x) % N;

	(i == 0) && (i = N)

	return i
end


#Checa se existe um elemento com valor x no vetor vet
function isInArray(array, x)
	for i in array
		(x == i) && (return true)
	end

	return false
end

#Retorna o número de conexões de uma rede small-world Newman-Watts definida por (N,p,k)
p_to_nc(p::Float64, N::Int, k::Int=4)  = Int(floor( k*N * (1+p) ))
nc_to_p(nc::Int, N::Int, k::Int=4) = Float64(nc - k*N) / (k*N)

export i_plus_x
export isInArray
export p_to_nc, nc_to_p


function list()
	println("Funções de Utils:\n")
	
	println("i_plus_x(i, x, N)")
	println("isInArray(array, x)")
	println("p_to_nc(p::Float64, N::Int, k::Int=4)")
	println("nc_to_p(nc::Int, N::Int, k::Int=4)")
end


end
